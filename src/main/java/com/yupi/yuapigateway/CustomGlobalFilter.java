package com.yupi.yuapigateway;

import com.example.yuapiclientsdk1.utils.SignUtils;
import com.yupi.yuapicommon.model.entity.InterfaceInfo;
import com.yupi.yuapicommon.model.entity.User;
import com.yupi.yuapicommon.service.InnerInterfaceInfoService;
import com.yupi.yuapicommon.service.InnerUserInterfaceInfoService;
import com.yupi.yuapicommon.service.InnerUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.reactivestreams.Publisher;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class CustomGlobalFilter implements GlobalFilter, Ordered {

    @DubboReference
    private InnerUserService innerUserService;

    @DubboReference
    private InnerInterfaceInfoService innerInterfaceInfoService;

    @DubboReference
    private InnerUserInterfaceInfoService innerUserInterfaceInfoService;

    private static final List<String> IP_WHITE_LIST = Arrays.asList("127.0.0.1");

    private static final String INTERFACE_HOST = "http://localhost:8123";
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("custom global filter");

        //1. 请求日志
        ServerHttpRequest request = exchange.getRequest();
        String path =  INTERFACE_HOST + request.getPath().value();
        String method = request.getMethod().toString();
        log.info("请求唯一标识" + request.getId());
        log.info("请求路径"+ request.getPath().value());
        log.info("请求方法" + request.getMethod());
        log.info("请求参数" + request.getQueryParams());
        String sourceAddress = request.getLocalAddress().getHostString();
        log.info("请求来源地址" + sourceAddress);
        log.info("请求来源地址" + request.getRemoteAddress());

        //拿到响应对象
        ServerHttpResponse response = exchange.getResponse();
        // 2.访问控制 黑白名单
        if(!IP_WHITE_LIST.contains(sourceAddress)){
            response.setStatusCode(HttpStatus.FORBIDDEN);
            return response.setComplete();
        }

        // 3. 用户鉴权(判断 ak sk 是否合法)
        HttpHeaders headers = request.getHeaders();
        String accessKey = headers.getFirst("accessKey");
        String nonce = headers.getFirst("nonce");
        String timestamp = headers.getFirst("timestamp");
        String sign = headers.getFirst("sign");
        String body = headers.getFirst("body");

        User invokeUser = null;
        try{
            invokeUser = innerUserService.getInvokeUser(accessKey);
        }catch (Exception e){
            log.error("getInvokeUser error", e);
        }
        if(invokeUser==null){
            return handleNoAuth(response);
        }
//        if(!"yupi".equals(accessKey)){
//            return handleNoAuth(response);
//        }

        if(Long.parseLong(nonce) > 10000){
            return handleNoAuth(response);
        }

        //时间和当前时间不能超过5分钟
        Long currentTime = System.currentTimeMillis() / 1000;
        final Long FIVE_MINUTES = 60 * 5L;
        if(currentTime - Long.parseLong(timestamp) >= FIVE_MINUTES){
            return handleNoAuth(response);
        }

//        String serverSign = SignUtils.genSign(body, "abc");
//        if(!sign.equals(serverSign)){
//            return handleNoAuth(response);
//        }

        String secretKey = invokeUser.getSecretKey();
        String serverSign = SignUtils.genSign(body,secretKey);
        if(sign==null || !sign.equals(serverSign)){
            return handleNoAuth(response);
        }

        // 4. 请求的模拟接口是否存在
        // todo 从数据库中查询模拟接口是否存在，以及请求的方式是否匹配(还可以校验请求参数)
        InterfaceInfo interfaceInfo = null;
        try{
            interfaceInfo = innerInterfaceInfoService.getInterfaceInfo(path, method);
        }catch (Exception e){
            log.error("getInterfaceInfo error", e);
        }

        if(interfaceInfo==null){
            return handleNoAuth(response);
        }

        // 5. 请求转发，调用模拟接口
        Mono<Void> filter = chain.filter(exchange);
        //调用成功后要输出一个响应日志
        log.info("响应" + response.getStatusCode());

        //6. 响应日志
        return handleResponse(exchange,chain, interfaceInfo.getId(), invokeUser.getId());
//        //7. todo 调用成功，接口调用次数 + 1
//        if(response.getStatusCode()==HttpStatus.OK){
//
//        }else {
//            return handleInvokeError(response);
//        }
//        //8. 调用失败，返回一个规范的错误码
//        return filter;


    }

    @Override
    public int getOrder() {
        return -1;
    }

    public Mono<Void> handleNoAuth(ServerHttpResponse response){
        response.setStatusCode(HttpStatus.FORBIDDEN);
        return response.setComplete();
    }

    public Mono<Void> handleInvokeError(ServerHttpResponse response){
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        return response.setComplete();
    }

    public Mono<Void> handleResponse(ServerWebExchange exchange, GatewayFilterChain chain, long interfaceInfoId, long userId){
        try {
            ServerHttpResponse originalResponse = exchange.getResponse();
            DataBufferFactory bufferFactory = originalResponse.bufferFactory();
            HttpStatus statusCode = originalResponse.getStatusCode();

            if(statusCode==HttpStatus.OK){
                ServerHttpResponseDecorator decoratorResponse = new ServerHttpResponseDecorator(originalResponse){
                    @Override
                    public Mono<Void> writeWith(Publisher<? extends DataBuffer> body){
                       // log.info("body instanceof Flux:{}", (body instanceof Flux));
                        System.out.println("日志");

                        if(body instanceof Flux){
                            System.out.println("日志1");
                            Flux<? extends DataBuffer> fluxBody = Flux.from(body);

                            return super.writeWith(fluxBody.map(dataBuffer -> {
                                try{
                                    innerUserInterfaceInfoService.invokeCount(interfaceInfoId, userId);
                                }catch (Exception e){
                                    log.error("invokeCount error", e);
                                }

                                byte[] content = new byte[dataBuffer.readableByteCount()];
                                dataBuffer.read(content);
                                DataBufferUtils.release(dataBuffer);

                                StringBuilder sb2 = new StringBuilder(200);
                                List<Object> rspArgs = new ArrayList<>();
                                rspArgs.add(originalResponse.getStatusCode());
                                String data = new String(content, StandardCharsets.UTF_8);
                                //7. todo 调用成功，接口调用次数 + 1 invokeCount
                                sb2.append(data);
                                log.info("响应结果" + data);
                                return bufferFactory.wrap(content);
                            }));
                        } else{
                            log.error("<---{} 响应code异常", getStatusCode());
                        }

                        return super.writeWith(body);
                    }
                };

                return chain.filter(exchange.mutate().response(decoratorResponse).build());


            }
            return chain.filter(exchange);

        } catch (Exception e) {
            log.error("网关处理响应异常" + e);
            return chain.filter(exchange);
        }
    }
}
